import 'package:flutter/material.dart';
import 'package:clonetinder/functions/alertFunction.dart';
import 'package:clonetinder/utils/constants.dart';
import 'package:clonetinder/widgets/appBar.dart';
import 'package:clonetinder/widgets/bottom.dart';
import 'package:swipe_cards/swipe_cards.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<SwipeItem> _swipeItems = <SwipeItem>[];
  MatchEngine? _matchEngine;
  List<String> names = [
    'Chaien Dz',
    'Doraemon',
    'Naruto',
    'Nobita',
    'Zổ',
    'Dog',
    'Alaska',
    'White Dog',
    'Bug Dog'
  ];

  @override
  void initState() {
    for (int i = 0; i < names.length; i++) {
      _swipeItems.add(SwipeItem(
          content: Content(text: names[i]),
          likeAction: () {
            actions(context, names[i], 'Liked');
          },
          nopeAction: () {
            actions(context, names[i], 'Rejected');
          },
          superlikeAction: () {
            actions(context, names[i], 'SuperLiked');
          }));
    }
    _matchEngine = MatchEngine(swipeItems: _swipeItems);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            SizedBox(height: 70),
            TopPage(),
            Expanded(
                child: Container(
                  child: SwipeCards(
                    matchEngine: _matchEngine!,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        alignment: Alignment.bottomLeft,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(images[index]),
                                fit: BoxFit.cover),
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10)),
                        padding: EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              names[index],
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      );
                    },
                    onStackFinished: () {
                      return ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text('The List is over')));
                    },
                  ),
                )),
            BottomPage()
          ],
        ),
      ),
    );
  }
}

class Content {
  final String? text;
  Content({this.text});
}




