
import 'package:clonetinder/screens/Home.dart';
import 'package:clonetinder/screens/Resport.dart';
import 'package:clonetinder/screens/Settings.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool isObscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "tinder",
          style: TextStyle(
              fontSize: 25,
              color: Colors.red
          ),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Image.asset(
            'assets/images/tinder.jpg',
            width: 35,
            height: 35,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Home())
            );
          },
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.shield,
                color: Colors.redAccent,),
              onPressed: () {
                final snackBar = SnackBar(
                  content: SingleChildScrollView(
                    child: Container(
                      height: 300,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Bộ Công Cụ An Toàn',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                              SizedBox(width: 20),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                              icon: Icon(Icons.flag,
                              color: Colors.redAccent,),
                              onPressed: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => ResportPage())
                                );
                              },),
                              Text(
                                'BÁO CÁO AI ĐÓ',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                ),
                              ),
                              SizedBox(width: 500),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(Icons.shield,
                                  color: Colors.blueAccent),
                              Text(
                                'TRUY CẬP TRUNG TÂM AN\nTOÀN',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                ),
                              ),
                              SizedBox(width: 110),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  backgroundColor: Colors.white,
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
          ),
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.redAccent,),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Settings())
              );
            },
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15, top: 20, right: 15),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Center(
                child: Stack(
                  children: [
                    Container(
                        width: 130,
                        height: 130,
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 4, color: Colors.white),
                            boxShadow: [
                              BoxShadow(
                                  spreadRadius: 2,
                                  blurRadius: 10,
                                  color: Colors.black.withOpacity(0.1)
                              )
                            ],
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                    'https://media.istockphoto.com/id/1348728804/vi/anh/ch%C3%A0ng-trai-tr%E1%BA%BB-%C4%91i-b%E1%BB%99-%C4%91%C6%B0%E1%BB%9Dng-d%C3%A0i-ch%E1%BB%A5p-%E1%BA%A3nh-ch%C3%A2n-dung-selfie-tr%C3%AAn-%C4%91%E1%BB%89nh-n%C3%BAi-ch%C3%A0ng-trai-h%E1%BA%A1nh-ph%C3%BAc-m%E1%BB%89m.jpg?s=2048x2048&w=is&k=20&c=DJmSQY6K0Hi7kopR4hU7oX24-xrouK_8eD5GiqGRtSk='
                                )
                            )
                        )
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Colors.white
                            ),
                            color: Colors.blue
                        ),
                        child: Icon(
                          Icons.edit,
                          color: Colors.white,
                        ),
                      ),
                    ),

                  ],
                ),
              ),
              SizedBox(height: 30,),
              buildTextField(
                  "Full Name", "VHau", false),
              buildTextField(
                  "Email", "hau@donga.edu.vn", false),
              buildTextField(
                  "Password", "******", true),
              buildTextField(
                  "Location", "Da Nang", false),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  OutlinedButton(onPressed: () {},
                      child: Text(
                        "Cancel",
                        style: TextStyle(
                            fontSize: 15,
                            letterSpacing: 2,
                            color: Colors.black
                        ),
                      ),
                      style: OutlinedButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                            horizontal: 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                      )
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: Text("Save",
                      style: TextStyle(
                        fontSize: 15,
                        letterSpacing: 2,
                        color: Colors.white,
                      ),),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.blue,
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))
                    ),
                  )
                ],
              )
            ],
          ),
        ),

      ),
    );
  }

  Widget buildTextField(String labelText, String placeholder,
      bool isPasswordTextField) {
    return Padding(
      padding:
      EdgeInsets.only(bottom: 30),
      child: TextField(
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField ?
            IconButton(
              icon: Icon(Icons.remove_red_eye, color: Colors.grey),
              onPressed: () {},
            ) : null,
            contentPadding: EdgeInsets.only(bottom: 5),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.grey
            )
        ),
      ),
    );
  }
}
