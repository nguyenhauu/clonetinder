import 'package:clonetinder/screens/Profile.dart';
import 'package:clonetinder/screens/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Settings extends StatelessWidget {
  bool vaNotify1 = false;
  bool vaNotify2 = true;
  bool vaNotify3 = false;

  onChangeFunction1(bool newValue1) {
    setState(() {
      vaNotify1 = newValue1;
    });
  }

  onChangeFunction2(bool newValue2) {
    setState(() {
      vaNotify1 = newValue2;
    });
  }

  onChangeFunction3(bool newValue3) {
    setState(() {
      vaNotify1 = newValue3;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('tinder',
          style: TextStyle(
              fontSize: 25,
              color: Colors.redAccent
          ),),
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Image.asset(
              'assets/images/tinder.jpg',
              width: 35,
              height: 35,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.check,
              color: Colors.redAccent,
            ),)
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: [
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Icon(
                  Icons.person,
                  color: Colors.blue,
                ),
                SizedBox(
                  width: 10,
                ),
                Text("Thiết lập tài khoản",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                  ),)
              ],
            ),
            Divider(
              height: 20,
              thickness: 1,
            ),
            SizedBox(
              height: 10,
            ),
            buiAccountOption(context, "Số điện thoại"),
            buiAccountOption(context, "Tài khoản đã kết nối"),
            buiAccountOption(context, "Email"),
            buiAccountOption(context, "Địa điểm"),
            buiAccountOption(context, "Phạm vi tối ta"),
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Icon(Icons.volume_up_outlined,
                  color: Colors.blue,),
                SizedBox(
                  width: 10,
                ),
                Text("Thông báo",
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                  ),)
              ],
            ),
            Divider(height: 20, thickness: 1,),
            SizedBox(
              height: 10,
            ),
            buildNotificationOption("Chủ đề", vaNotify1, onChangeFunction1),
            buildNotificationOption("Tài khoản", vaNotify2, onChangeFunction2),
            buildNotificationOption(
                "Opportunity", vaNotify3, onChangeFunction3),
            SizedBox(height: 50),
            Center(
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)
                    )
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage())
                  );
                },
                child: Text("Sign Out",
                  style: TextStyle(
                      fontSize: 16,
                      letterSpacing: 2.2,
                      color: Colors.black
                  ),),
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding buildNotificationOption(String title, bool value,
      Function onChangeMethod) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title, style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Colors.grey[600]
          ),),
          Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              activeColor: Colors.redAccent,
              trackColor: Colors.black,
              value: value,
              onChanged: (bool newValue) {
                onChangeMethod(newValue);
              },
            ),
          )
        ],
      ),
    );
  }

  GestureDetector buiAccountOption(BuildContext context, String title) {
    return GestureDetector(
      onTap: () {
        showDialog(context: context, builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Option 1"),
                Text("Option 2")
              ],
            ),
            actions: [
              TextButton(onPressed: () {
                Navigator.of(context).pop();
              },
                  child: Text("Close"))
            ],
          );
        });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title, style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Colors.grey[600],
            ),),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.redAccent,
            )
          ],
        ),
      ),
    );
  }

  void setState(Function() param0) {}
}