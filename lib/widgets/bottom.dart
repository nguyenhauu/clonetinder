import 'package:clonetinder/screens/Home.dart';
import 'package:clonetinder/screens/Resport.dart';
import 'package:clonetinder/screens/Search.dart';
import 'package:clonetinder/screens/Profile.dart';
import 'package:flutter/material.dart';

class BottomPage extends StatefulWidget {
  const BottomPage({Key? key}) : super(key: key);

  @override
  _BottomPageState createState() => _BottomPageState();
}

class _BottomPageState extends State<BottomPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      // color: Colors.green,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Home())
                );
              },
              child: Container(
                child: Icon(
                  Icons.refresh, color: Colors.amber,
                    size: 40,
                ),
              ),
            ),
            InkWell(
              onTap: (){
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(builder: (context) => Home())
                // );
              },
              child: Container(
                child: Icon(
                    Icons.close, color: Colors.redAccent,
                    size: 40,
                ),
              ),
            ),
            InkWell(
              onTap: (){
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(builder: (context) => Profile())
                // );
              },
              child: Container(
                child: Icon(
                    Icons.star, color: Colors.blue,
                  size: 40,
                ),
              ),
            ),
            InkWell(
              onTap: (){
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(builder: (context) => SearchPage())
                // );
              },
              child: Container(
                child: Icon(
                    Icons.favorite_outline_outlined, color: Colors.pink,
                  size: 40,
                ),
              ),
            ),
            InkWell(
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Profile())
                );
              },
              child: Container(
                child: Icon(
                    Icons.person, color: Colors.green,
                  size: 40,
                ),
              ),
            ),
      ]),
    );
  }
}

Widget buttonWidget(IconData icon, Color color) {
  return Container(
    height: 60,
    width: 60,
    decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        border: Border.all(color: color)),
    child: Icon(
      icon,
      color: color,
      size: 30,
    ),
  );
}