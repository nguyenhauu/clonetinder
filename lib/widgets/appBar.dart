
import 'package:clonetinder/screens/Notifications.dart';
import 'package:clonetinder/screens/chat.dart';
import 'package:clonetinder/screens/menu.dart';
import 'package:clonetinder/utils/NavBar.dart';
import 'package:flutter/material.dart';

import '../screens/Search.dart';

class TopPage extends StatefulWidget {
  const TopPage({Key? key}) : super(key: key);

  @override
  _TopPageState createState() => _TopPageState();
}

class _TopPageState extends State<TopPage> {
  @override
  Widget build(BuildContext context) {
    return
      Container(
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NavBar())
              );
            },
            child: Container(
              child:
              Image.asset('assets/images/tinder.jpg'),
                width: 50,
                height: 50,
              ),
            ),
          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SearchPage())
              );
            },
            child: Container(
              child: Icon(
                Icons.search, color: Colors.redAccent,
                size: 40,
              ),
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChatPage())
              );
            },
            child: Container(
              child: Icon(
                Icons.message, color: Colors.redAccent,
                size: 40,
              ),
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NotificationPage())
              );
            },
            child: Container(
              child: Icon(
                Icons.notifications, color: Colors.redAccent,
                size: 40,
              ),
            ),
          ),
        ]
      ),
      );
  }
}
